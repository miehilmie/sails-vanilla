var app = angular.module('services.auth', []);

app.factory('principal', ['$q', '$http', '$timeout', '$rootScope', 'config',
  function($q, $http, $timeout, $rootScope, config) {
    var _identity = undefined,
      _authenticated = false;

    return {
      isIdentityResolved: function() {
        return angular.isDefined(_identity);
      },
      isAuthenticated: function() {
        return _authenticated;
      },
      isInRole: function(role) {
        if (!_authenticated || !_identity.role) return false;

        return _identity.role.indexOf(role) != -1;
      },
      isInAnyRole: function(roles) {
        if(!_authenticated && roles.indexOf('guest') != -1) return true;
        else if(_authenticated && roles.indexOf('guest') != -1) return false;
        else if (!_authenticated || !_identity.role) return false;

        return roles.indexOf('*') != -1 || roles.indexOf(_identity.role) != -1;
      },
      authenticate: function(identity) {
        _identity = identity;
        _authenticated = identity != null;

        // expose user object to all
        config.currentUser = identity;
      },
      identity: function(force) {
        var deferred = $q.defer();

        if (force === true) _identity = undefined;

        // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
        if (angular.isDefined(_identity)) {
          deferred.resolve(_identity);

          return deferred.promise;
        }

        var self = this;
        // otherwise, retrieve the identity data from the server, update the identity object, and then resolve.
        if(typeof config.currentUser === 'object') {
            self.authenticate(config.currentUser);
            deferred.resolve(config.currentUser);
        } else {
            self.authenticate(null);
            deferred.resolve(null);
        }

        return deferred.promise;
      }
    };
  }
]);
// authorization service's purpose is to wrap up authorize functionality
// it basically just checks to see if the principal is authenticated and checks the root state
// to see if there is a state that needs to be authorized. if so, it does a role check.
// this is used by the state resolver to make sure when you refresh, hard navigate, or drop onto a
// route, the app resolves your identity before it does an authorize check. after that,
// authorize is called from $stateChangeStart to make sure the principal is allowed to change to
// the desired state
app.factory('authorization', ['$rootScope', '$state', 'principal',
  function($rootScope, $state, principal) {
    return {
      authorize: function() {
        return principal.identity()
          .then(function() {
            var isAuthenticated = principal.isAuthenticated();
            if ($rootScope.toState.data && $rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !principal.isInAnyRole($rootScope.toState.data.roles)) {
              if (isAuthenticated) {
                $state.go('home');
              } // user is signed in but not authorized for desired state
              else {
                // user is not authenticated. stow the state they wanted before you
                // send them to the signin state, so you can return them when you're done
                $rootScope.returnToState = $rootScope.toState;
                $rootScope.returnToStateParams = $rootScope.toStateParams;

                // now, send them to the signin state so they can log in
                $state.go('login');
              }
            }
          });
      }
    };
  }
]);

// this will be id for your user roles
app.constant('USER_ROLES', {
    admin: 'admin',
    user: 'normal',
    authenticated: '*',
    guest: 'guest'
});

app.factory('AuthService', ['$http', '$q',
    function($http, $q) {
        var authService = {};

        authService.login = function(credentials) {
            var deferred = $q.defer()
            $http.post('/api/login', credentials).then(function(res) {
                if (!res.data.user) {
                    deferred.reject(res.data);
                } else {
                    deferred.resolve(res.data);
                }
            }, function(res) {
                deferred.reject();
            });
            return deferred.promise;
        };

        authService.logout = function() {
            var deferred = $q.defer()
            $http.get('/api/logout').then(function(res) {
                deferred.resolve(res.data);
            }, function(res) {
                deferred.reject();
            });
            return deferred.promise;
        };

        return authService;
    }
]);