angular.module( 'sailng.auth', [])
.config( ['$stateProvider', 'USER_ROLES', function config( $stateProvider, USER_ROLES ) {
    $stateProvider.state( 'login', {
        parent: 'base',
        url: '/login',
        views: {
            "main": {
                controller: 'LoginCtrl',
                templateUrl: 'auth/login.tpl.html'
            }
        },
        data: {
            roles: [USER_ROLES.guest]
        }
    });

    $stateProvider.state( 'register', {
        parent: 'base',
        url: '/register',
        views: {
            "main": {
                controller: 'RegisterCtrl',
                templateUrl: 'auth/register.tpl.html'
            }
        },
        data: {
            roles: [USER_ROLES.guest]
        }
    });
}])

.controller( 'LoginCtrl', ['$scope', 'titleService', 'AuthService', 'principal', '$state', function HomeController( $scope, titleService, AuthService, principal, $state) {
    titleService.setTitle('Login');

    $scope.login = function() {
        AuthService.login({
            email: $scope.email,
            password: $scope.password
        }).then(function(data) {
            if(data.user) {
                principal.authenticate(data.user);
                return window.location = '/home';
            }
            $scope.error = 'Invalid username / password';
        }, function(data) {
            $scope.error = 'Invalid username / password';
        })
    }
}])

.controller( 'RegisterCtrl', ['$scope', 'titleService', 'UserModel', 'principal', '$state', function HomeController( $scope, titleService, UserModel, principal, $state) {
    titleService.setTitle('Register');

    $scope.register = function() {
        UserModel.create({
            username: $scope.username,
            email: $scope.email,
            password: $scope.password,
            name: $scope.name,
            role: $scope.role
        }).then(function(data) {
            $state.go('login');
        }, function(data) {
        })
    }
}]);