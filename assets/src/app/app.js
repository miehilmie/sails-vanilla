angular.module( 'sailng', [
    'ui.router',
    'lodash',
    'angularMoment',
    'ui.bootstrap',
    'templates-app',
    'services',
    'models',
    'directive.blink',
    'sailng.header',
    'sailng.home',
    'sailng.auth',
    'sailng.about',
    'sailng.users'

])
.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function myAppConfig ( $stateProvider, $urlRouterProvider, $locationProvider ) {

    $urlRouterProvider.otherwise(function ($injector, $location) {

        if ($location.$$url === '/') {
            window.location = '/home';
        } else {
            window.location = $location.$$absUrl
        }

    });

    $stateProvider.state('base', {
        'abstract': true,
        views: {
            "main": {
                template: '<div ui-view="main"></div>'
            }
        },
        resolve: {
          authorize: ['authorization',
            function(authorization) {
              return authorization.authorize();
            }
          ]
        }
    });

    $locationProvider.html5Mode(true);
}])
.run(['$http', '$state','$rootScope', '$filter', '$location', 'authorization', 'principal', '$window',
    function($http, $state, $rootScope, $filter, $location, authorization, principal, $window) {
        moment.locale('en');
        $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams, fromState, fromStateParams) {
        $rootScope.toState = toState;
        $rootScope.toStateParams = toStateParams;
        $rootScope.fromState = fromState;
        $rootScope.fromStateParams = fromStateParams;
        if (principal.isIdentityResolved()) {
            authorization.authorize();
        }
     });

    $rootScope.goBack = function() {
        // if browser support window history
        if($window.history) {
            $window.history.back();

        // else use $state.go to navigate previous state
        } else if($rootScope.fromState && $rootScope.fromState.name) {
            $state.go($rootScope.fromState.name, $rootScope.fromStateParams);
        }
    }
}])

.controller( 'AppCtrl',['$scope', 'config', function AppCtrl ( $scope, config ) {
    config.currentUser = window.currentUser;
}]);
