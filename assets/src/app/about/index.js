/**
 * Created by mmarino on 9/5/2014.
 */
angular.module( 'sailng.about', [
])
.config( ['$stateProvider', 'USER_ROLES', function config( $stateProvider, USER_ROLES ) {
    $stateProvider.state( 'about', {
        parent: 'base',
        url: '/about',
        views: {
            "main": {
                controller: 'AboutCtrl',
                templateUrl: 'about/index.tpl.html'
            }
        }
    });
}])

.controller( 'AboutCtrl',['$scope', 'titleService', function AboutController( $scope, titleService ) {
    titleService.setTitle('About');
}]);