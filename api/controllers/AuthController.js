/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var passport = require('passport');

module.exports = {
	login: function(req, res) {
        passport.authenticate('local', function(err, user, info) {
            if ((err) || (!user)) {
                return res.send({
                    message: 'Maklumat login / kata laluan salah!',
                    user: user
                });
            }
            req.logIn(user, function(err) {
                if (err) return res.serverError(err);

                return res.send({
                    user: user
                });
            });
        })(req, res);
    },

    logout: function(req, res) {
        req.logout();
        return res.redirect('/home');
    },
};

