/**
 * Instantiate Waterline model for each Sails Model,
 * then start the ORM.
 *
 * @param {Object}    modelDefs
 * @param {Function}  cb
 *              -> err  // Error, if one occurred, or null
 */
 var mongoose = require('mongoose');

module.exports = function howto_buildORM(sails) {
  return function buildORM(modelDefs, cb) {

    // make all models as mongoose model and expose to global
    _.each(modelDefs, function(thisModel, modelID) {
      var schema = thisModel(mongoose);
      global[thisModel.globalId] = sails.models[modelID] = mongoose.model(thisModel.globalId, schema);
    });

    // If sails is already exiting due to previous errors, bail out.
    // This could happen if another hook fails to load.
    if (sails._exiting) {return cb("SAILS EXITING");}

    // -> Instantiate ORM in memory.
    // -> Iterate through each model definition:
    //    -> Create a proper Waterline Collection for each model
    //    -> then register it w/ the ORM.
    sails.log.verbose('Starting ORM...');

    buildConnection(sails.config.connections[sails.config.mongoose.connection], cb);

    function buildConnection(options, cb) {
      var uri = 'mongodb://';

      if(options.hosts && _.isArray(options.hosts)) {
        _.each(options.hosts, function(conn, index) {
          if(index !== 0) {
            uri += ',';
          }
          if(conn.host) {
            uri += conn.host;
          }
          if(conn.port) {
            uri += ':' + conn.port
          } else if(options.port) {
            uri += ':' + options.port
          }
        })
      } else {
        uri += options.host;
        if(options.port) {
          uri += ':' + options.port;
        }
      }
      uri += '/';
      uri += options.db;

      var connectionOptions = {};
      if(options.user) connectionOptions.user = options.user;
      if(options.pass) connectionOptions.pass = options.pass;
      console.log(uri);
      mongoose.connect(uri, connectionOptions, cb);
    }
  };
};
