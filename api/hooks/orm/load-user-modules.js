/**
 * Module dependencies.
 */
var async = require('async');
var _ = require('lodash');

module.exports = function howto_lookupUserModules (sails) {

  return function lookupUserModules (cb) {

    sails.log.verbose('Loading the app\'s models and adapters...');
    async.auto({

      models: function(cb) {
        sails.log.verbose('Loading app models...');

        // sails.models = {};

        // Load app's model definitions
        // Case-insensitive, using filename to determine identity
        sails.modules.loadModels(function modulesLoaded(err, modules) {
          if (err) return cb(err);
          sails.models = _.extend(sails.models || {}, modules);
          return cb();
        });
      }

    }, cb);
  };

};
