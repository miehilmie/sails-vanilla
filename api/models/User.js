/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var uniqueValidator = require('mongoose-unique-validator');
var bcrypt = require('bcrypt-nodejs');

module.exports = function(mongoose) {
    var Schema = mongoose.Schema;

    var schema = new Schema({
        email: {
            type: String,
            unique: true
        },
        password: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        role: {
            type: String,
            enum: ['admin', 'normal']
        }
    });

    schema.pre('save', function(next) {
        var self = this;

        bcrypt.genSalt(10, function(err, salt) {
           bcrypt.hash(self.password, salt,null, function(err, hash) {
               if (err) return next(err);

               self.password = hash;
               next();
           });
       });
    });

    schema.plugin(uniqueValidator);
    return schema;
};