module.exports = function (gulp, plugins) {
  gulp.task('build', function (cb) {
    plugins.sequence(
      'compileAssets',
      'copy:image',
      'linkAssetsBuild',
      'clean:build',
      'copy:build',
      cb
    );
  });
};
