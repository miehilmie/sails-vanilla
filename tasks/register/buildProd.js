module.exports = function (gulp, plugins) {
  gulp.task('buildProd', function(cb) {
    plugins.sequence(
      'compileAssets',
      'concat:js',
      'concat:css',
      'uglify:dist',
      'cssmin:dist',
      'linkAssetsBuildProd',
      'images',
      'clean:build',
      'copy:build',
      cb
    );
  });
};
