module.exports = function (gulp, plugins) {
  gulp.task('compileAssets', function(cb) {
    plugins.sequence(
      'clean:dev',
      'bower:install',
      'bower:clean',
      'bower:copy',
      'copy:devng',
      'html2js:dev',
      'sass:dev',
      'copy:dev',
      'coffee:dev',
      cb
    );
  });
};
